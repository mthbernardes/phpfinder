import os
import re
import pathlib
from termcolor import colored

class findPHP(object):
    def __init__(self,project_path,extension):
        self.all_files = list() 
        for root, dirs, files in os.walk(project_path):
            for filename in files:
                if filename.lower().endswith(extension):
                    self.all_files.append(os.path.join(root, filename))
        print('Total PHP files found: {}'.format(len(self.all_files)))

    def search(self,function,regex=False):
        function = "{}\(.*\)".format(function) if not regex else function
        pattern = re.compile(function,flags=re.IGNORECASE | re.MULTILINE)
        for filename in self.all_files:
            results = set()
            line_number = 1
            for line in open(filename).readlines():
                matchs = pattern.findall(line)
                if matchs: 
                    try:
                        for match in matchs:
                            result = pattern.sub(colored(match,'magenta'), line)
                            results.add('{}:{}:{}'.format(colored(filename,'green'),colored(line_number,'yellow'),result.strip()))
                    except:
                        pass
                line_number += 1
            for r in results:print(r)

