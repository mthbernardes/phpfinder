import os

class PHPCategories:
    def __init__(self,):
        self.HOME = os.path.abspath(os.path.join(os.path.dirname( os.path.join( os.path.abspath(__file__) ) ),'../'))
        self.PHP_CATEGORIES_PATH = os.path.join(self.HOME,'php_functions')
        self.PHP_CATEGORIES = os.listdir(self.PHP_CATEGORIES_PATH)

    def listCategories(self,):
        return self.PHP_CATEGORIES

    def describeCategories(self,function):
        if function in self.PHP_CATEGORIES:
            return [function.strip() for function in open('{}/{}'.format(self.PHP_CATEGORIES_PATH,function)).readlines()]
